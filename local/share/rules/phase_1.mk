### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Mar 12 19:11:26 2013 (+0100)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

PRJ1 ?= tetraploid
TAG ?= HAPLO1
GENOME ?= $(shell seq -s " " 4)
FILE_LIST ?=



# prepare externs for link genome
externs.mk:
	> $@; \
	GENOMES="FILE_LIST ="; \
	for N in $(GENOME); do \
	printf "extern ../../../../transcriptome_simulation/dataset/$(PRJ1)/phase_1/GNOM$${N}/GNOM.fq.cleaned as STRAIN$${N}_FASTQ\n" >> $@; \
	GENOMES="$${GENOMES} STRAIN$${N}_FASTQ"; \
	done; \
	printf "$${GENOMES}" >> $@;



include externs.mk


# link original data in fasta format
.SECONDEXPANSION:
strain%.fastq: $$(STRAIN%_FASTQ)
	ln -sf $< $@

strain%.strain: $$(STRAIN%_STRAIN)
	ln -sf $< $@

strain%.xml: $$(STRAIN%_XML)
	ln -sf $< $@


# prepare MIRA input files
individual.fastq: $(addsuffix .fastq, $(addprefix strain, $(GENOME))) # strain1.fastq strain2.fastq strain3.fastq strain4.fastq ..
	cat $^ \
	| fastq2tab \
	| bawk -v tag=$(TAG) '!/^[$$,\#+]/ { \
	if (length($$2)) { printf "%s_%s\t%s\t%s\n", tag, $$1, $$2, $$3; } \
	}' \
	| tab2fastq >$@

individual.strain: individual.fastq
	fastq2tab <$< \
	| bawk ' !/^[$$,\#+]/ { \
	split($$1,a,"_"); \
	printf "%s\t%s\n", $$1, a[1]; \
	}' > $@


ALL   += individual.fastq \
	 individual.strain


INTERMEDIATE +=



CLEAN += individual.fastq \
	 individual.strain \
	 $(addsuffix .fastq, $(addprefix strain, $(GENOME))) \
	 externs.mk



######################################################################
### phase_1.mk ends here
